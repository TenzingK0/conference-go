from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_picture(city, state):
    pexels_url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(pexels_url, headers=headers, params=params)
    photo = json.loads(response.content)
    picture = {
        "picture": photo["photos"][0]["src"]["original"]
    }
    try:
        return picture
    except:
        return {"picture": None}


def get_weather_data(city, state):
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response_1 = requests.get(geocoding_url, params=params)
    location = json.loads(response_1.content)
    try:
        lat = location[0]["lat"]
        lon = location[0]["lon"]
    except:
        return {
            "temp": None,
            "description": None,
        }
    weather_map_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response_2 = requests.get(weather_map_url, params=params)
    weather = json.loads(response_2.content)
    weather_data = {
        "temp": str(weather["main"]["temp"]) + "F",
        "description": weather["weather"][0]["description"],
    }
    try:
        return weather_data
    except:
        return {
            "temp": None,
            "description": None,
        }
